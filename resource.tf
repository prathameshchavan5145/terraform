#declaring provider
provider "aws" {
    region = "eu-north-1"
}

#declaring variables
variable "image_id" {
    default = "ami-0014ce3e52359afbd"
}
variable "type" {
    default = "t2.micro"
}
variable "key" {
    default = "webhost"
}
variable "sg" {
    default = ["sg-0f04dea65b8a19ed9"]    
}
#configuration for launch instance without variables
resource "aws_instance" "my_resource" {
    ami = "ami-0014ce3e52359afbd"
    instance_type = "t2.micro"
    key_name = "webhost"   
    vpc_security_group_ids = ["sg-0f04dea65b8a19ed9"]
    tags = {
      Name = "terraform"
    }
  
}
#configuration for launch instance usning variables
resource "aws_instance" "my_resource_variable" {
    ami = var.image_id
    instance_type = var.type
    key_name = var.key
    vpc_security_group_ids = var.sg
    tags = {
      Name = "terraform-variables"
      env = "practice"
    }
}