provider "aws" {
  region = "ap-south-1"
}
resource "aws_security_group" "my_security" {
  name = "prathamesh"
  description = "security"
  vpc_id = "vpc-0e5c5c2702dcf7c65"
  ingress {
    protocol = "TCP"
    from_port = 22
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    protocol = "TCP"
    from_port = 80
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "for" {
  ami = "ami-03f4878755434977f"
  instance_type = "t2.micro"
  key_name = "prathamesh"
  vpc_security_group_ids = [ "aws_security_group.my_security" ]
  tags = {
    Name = "prathamesh"
    env = "dev"
  }
}